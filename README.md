# SlimeCore
This mod add a sentinent slimes race with a different types of cores that support different type of liquids to form their body.
For example a slimes with a polar liquid core can be made of water, nonpolar cores support organic liquids as chemfuel etc.

If you know how to make this mod better - you can contact me:
1) Telegram: @Leonovers
2) Discord: Leonovers#7150
# Key Concepts
There are 3 key concepts
1) Voracity. Slimes can dissolve almost everything and keep as nutrients in liquid they made of. So dirt on the floor is also food for them, as items or even other pawns.
2) Morphing. They can change their body at will in a relatively short time using nutrients. Also they can vary in sizes, which is affected by their volume of liquid in body. Growing in volume will add them more mass(so more strength, bodyparts hitpoints and inventory capacity).
3) Core types and materials. Polar and nonpolar cores for polar and nonpolar liquids. Liquid also affects mass, and ther defences, work speed and other things as flammability, comfy temperatures, and special effects, as making everything around in fire and burning alive any pawn that close enough and don't have heat resistance.

Other things may and will vary.

![image.png](./image.png)
